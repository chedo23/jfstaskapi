﻿using System.Collections.Generic;

namespace JfsTaskApi.Domain.Models
{
    public class Accounts : IModel
    {
        public int Id { get; set; }

        public List<Balances> Balances { get; set; }

        public List<Payments> Payments { get; set; }

    }
}
