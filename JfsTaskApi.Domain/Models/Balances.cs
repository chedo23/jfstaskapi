﻿using System.Collections.Generic;

namespace JfsTaskApi.Domain.Models
{
    public class Balances : IModel
    {
        public int Id { get; set; }

        public int AccountId { get; set; }

        public Accounts Accounts { get; set; }

        public int Period { get; set; }

        public decimal InBalance { get; set; }

        public decimal Calculation { get; set; }

    }
}
