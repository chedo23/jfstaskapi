﻿using System;
using System.Collections.Generic;

namespace JfsTaskApi.Domain.Models
{
    public class Payments : IModel
    {
        public int Id { get; set; }

        public int AccountId { get; set; }

        public Accounts Accounts { get; set; }

        public DateTime PaymentDate { get; set; }

        public decimal PaymentSum { get; set; }

        public Guid PaymentGuid { get; set; }
    }
}
