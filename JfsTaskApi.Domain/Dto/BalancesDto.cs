﻿using System.Collections.Generic;

namespace JfsTaskApi.Domain.Dto
{
    public class BalancesDto : IDto
    {
        public int Id { get; set; }

        public int AccountId { get; set; }

        public List<AccountsDto> Accounts { get; set; }

        public int Period { get; set; }

        public decimal InBalance { get; set; }

        public decimal Calculation { get; set; }

    }
}
