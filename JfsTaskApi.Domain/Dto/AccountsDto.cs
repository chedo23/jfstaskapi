﻿using System.Collections.Generic;

namespace JfsTaskApi.Domain.Dto
{
    public class AccountsDto : IDto
    {
        public int Id { get; set; }

        public List<BalancesDto> Balances { get; set; }

        public List<PaymentsDto> Payments { get; set; }

    }
}
