﻿using System;
using System.Collections.Generic;

namespace JfsTaskApi.Domain.Dto
{
    public class PaymentsDto : IDto
    {
        public int Id { get; set; }

        public int AccountId { get; set; }

        public List<AccountsDto> Accounts { get; set; }

        public DateTime PaymentDate { get; set; }

        public decimal PaymentSum { get; set; }

        public Guid PaymentGuid { get; set; }
    }
}
