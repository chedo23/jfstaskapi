﻿using AutoMapper;
using JfsTaskApi.Domain.Dto;
using JfsTaskApi.Domain.Models;

namespace JfsTaskApi.DAL.Mapping
{
    public class DalMapping : Profile
    {
        public DalMapping()
        {
            this.MapDto();
        }

        private void MapDto()
        {
            this.CreateMap<Balances, BalancesDto>();

            this.CreateMap<Payments, PaymentsDto>();

            this.CreateMap<Accounts, AccountsDto>();
        }
    }
}
