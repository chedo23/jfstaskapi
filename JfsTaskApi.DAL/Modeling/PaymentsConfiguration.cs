﻿
using JfsTaskApi.Domain.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace JfsTaskApi.DAL.Modeling
{
    public class PaymentsConfiguration : IEntityTypeConfiguration<Payments>
    {
        public void Configure(EntityTypeBuilder<Payments> builder)
        {
            builder.Property(x => x.PaymentDate).HasDefaultValueSql("GETDATE()");
            builder
                .HasOne(x => x.Accounts)
                .WithMany(x => x.Payments)
                .OnDelete(DeleteBehavior.Cascade);
        }
    }
}
