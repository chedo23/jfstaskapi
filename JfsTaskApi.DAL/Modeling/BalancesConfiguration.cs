﻿
using JfsTaskApi.Domain.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace JfsTaskApi.DAL.Modeling
{
    public class BalancesConfiguration : IEntityTypeConfiguration<Balances>
    {
        public void Configure(EntityTypeBuilder<Balances> builder)
        {
            builder.Property(x => x.Period).HasDefaultValueSql("GETDATE()");
            builder
                .HasOne(x => x.Accounts)
                .WithMany(x => x.Balances)
                .OnDelete(DeleteBehavior.Cascade);
        }
    }
}
