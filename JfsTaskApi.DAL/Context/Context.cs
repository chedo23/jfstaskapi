﻿using JfsTaskApi.DAL.Modeling;
using JfsTaskApi.Domain.Models;
using Microsoft.EntityFrameworkCore;

namespace JfsTaskApi.DAL.Context
{
    public class Context : DbContext
    {
        public Context(DbContextOptions<Context> options) 
            : base(options)
        {
            Database.Migrate();
        }

        public DbSet<Accounts> account { get; set; }
        
        public DbSet<Balances> balance { get; set; }
        
        public DbSet<Payments> payment { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder) 
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.HasSequence<int>($"{nameof(Balances)}_seq").StartsAt(1).IncrementsBy(1);


            modelBuilder.HasSequence<int>($"{nameof(Payments)}_seq").StartsAt(1).IncrementsBy(1);


            modelBuilder.ApplyConfiguration<Balances>(new BalancesConfiguration());


            modelBuilder.ApplyConfiguration<Payments>(new PaymentsConfiguration());

        }

    }

    
}
